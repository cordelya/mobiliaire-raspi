# Mobiliaire-raspi
A Raspberry Pi flavored lightweight property inventory helper

> mobiliaire (n.): Middle French form of "mobiliary"; pertaining to furniture or movable property

Please see the main [Mobiliaire](https://github.com/Cordelya/mobiliaire) repository for full information about the application. This fork is now updated to be identical with the upstream repo. Once you have updated to the most recent release in this repo, you can switch to using the main repo for all future updates. If you are starting from scratch, just install directly from the main repo.

[Changelog](https://gitlab.com/Cordelya/mobiliaire#Changelog)

This self-hosted app is for assisting SCA branches (and other small non-profit entities) in managing both annual inventories and pack-in/pack-out at events. It is designed to be installed on a portable machine - to be taken to events and serve up web pages with inventory information without needing access to the Internet. A [$35 Raspberry Pi](https://www.raspberrypi.org/products/) fits the bill nicely here, but take my advice and spend the extra $10 for a [case](https://www.raspberrypi.org/products/raspberry-pi-4-case/) if you go that route. 

The camera support originially built-in for Raspberry Pi devices (using the official Raspberry Pi camera module) with [picamera](https://github.com/waveform80/picamera) available in this repo has been rolled into the upstream repository and USB webcam support has been added. Administrators should set the appropriate constant value for CAMERA, at the very bottom of `inventory/settings.py`. Camera support is turned off by default.

See it in action at https://cordelya.pythonanywhere.com (note: this will be whatever's in the main branch - changes in dev branches won't be available) minus the webcam support because that won't work there!

Raspberry Pi Cameras are available for purchase separately from the main Raspberry Pi board, or you may be able to find it as part of a bundle. There are several versions. You likely want the "Camera Module v2" version. Many models of USB webcam will also work natively with a Raspberry Pi (if using a USB webcam, select the "webcam" option for the CAMERA constant in settings.py.

You will also need to manufacture a way to mount the pi-camera board so it can be pointed at your photo booth or backdrop. While this overall is beyond the scope of this repository, you may be interested to know that pi-camera cases with LEGO brick attachment points are available for purchase from some suppliers.

Before you can run this repo with a pi-camera on a Raspberry Pi, you should view the [Raspi Camera Startup Guide](https://projects.raspberrypi.org/en/projects/getting-started-with-picamera) which will walk you through installing your camera, enabling it, and testing that it is working properly. You should test that the camera is working properly every time you plug it in to your Raspberry Pi board (remember - the blue tab faces toward the Ethernet/USB ports on most models, and always loosen the cable receiver clip before inserting or removing ribbon cables)
After you have done initial camera setup, you can test your camera on the command line by running:

````
$ raspistill -o testshot.jpg
````
If the camera is properly attached, you will see the camera preview for a few seconds, then it will disappear, and a photo named "testshot.jpg" should appear in the folder where you ran the test. You can remove the test photo once you have confirmed that it is there. 

Follow the install procedure in [Mobiliaire Wiki: Getting Started](https://github.com/Cordelya/mobiliaire/wiki/getStarted).
